#!/bin/bash

echo "Running in local docker image for running pipeline"

sudo docker run --rm\
                -v $PWD:/code\
                -i -t registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 /bin/bash\
                -c "cd /code;\
                    pip3 install -e .;\
                    cd pipelines;\
                    python3 primitive1_pipeline.py;\
                    python3 -m d3m runtime fit-score \
                            -p pipeline.json \
                            -r /code/datasets/seed_datasets_current/38_sick/TRAIN/problem_TRAIN/problemDoc.json \
                            -i /code/datasets/seed_datasets_current/38_sick/TRAIN/dataset_TRAIN/datasetDoc.json \
                            -t /code/datasets/seed_datasets_current/38_sick/TEST/dataset_TEST/datasetDoc.json \
                            -a /code/datasets/seed_datasets_current/38_sick/SCORE/dataset_SCORE/datasetDoc.json \
                            -o 38_sick_results.csv \
                            -O pipeline_run.yml;\
                    exit"


echo "Done!"
