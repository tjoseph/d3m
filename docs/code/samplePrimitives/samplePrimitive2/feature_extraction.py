from d3m import container
from d3m.primitive_interfaces import base, transformer
from d3m.metadata import hyperparams
from d3m.metadata import base as metadata_base

from samplePrimitives.config_files import config

import os
import typing
import logging
import numpy as np
from PIL import Image
import torch
import torch.nn as nn
import torchvision.transforms as transforms

__all__ = ('CNNFeatureExtract',)

logger  = logging.getLogger(__name__)
Inputs  = container.DataFrame
Outputs = container.DataFrame


class VGG(nn.Module):
    """
    Obtained from https://pytorch.org/docs/stable/_modules/torchvision/models/vgg.html#vgg16
    """
    def __init__(self, pretrained, num_classes=1000, include_top=False):
        super(VGG, self).__init__()
        # Make VGG features
        D = [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'M', 512, 512, 512, 'M', 512, 512, 512, 'M']
        self.include_top = include_top
        self.features    = self._make_layers(cfg=D)
        self.avgpool     = nn.AdaptiveAvgPool2d((7, 7))
        self.classifier  = nn.Sequential(nn.Linear(512 * 7 * 7, 4096),
                                         nn.ReLU(True),
                                         nn.Dropout(),
                                         nn.Linear(4096, 4096),
                                         nn.ReLU(True),
                                         nn.Dropout(),
                                         nn.Linear(4096, num_classes),
        )

        if not pretrained:
            self._initialize_weights()

    def forward(self, x):
        x = self.features(x)
        if self.include_top:
            x = self.avgpool(x)
            x = self.classifier(x)

        return x

    def _make_layers(self, cfg, batch_norm=False):
        layers = []
        in_channels = 3
        for v in cfg:
            if v == 'M':
                layers += [nn.MaxPool2d(kernel_size=2, stride=2)]
            else:
                conv2d = nn.Conv2d(in_channels, v, kernel_size=3, padding=1)
                if batch_norm:
                    layers += [conv2d, nn.BatchNorm2d(v), nn.ReLU(inplace=True)]
                else:
                    layers += [conv2d, nn.ReLU(inplace=True)]
                in_channels = v

        return nn.Sequential(*layers)

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.Linear):
                nn.init.normal_(m.weight, 0, 0.01)
                nn.init.constant_(m.bias, 0)


class WeightsDirPrimitive:
    """
    Creates a directory for weights if not present in local.
    """
    def __init__(self):
        self._there = False

    @staticmethod
    def _weights_data_dir(dir_name='/static'):
        if not os.path.isdir(dir_name):
            try:
                os.mkdir(dir_name)
            except FileNotFoundError or FileExistsError or PermissionError:
                try:
                    os.mkdir('/static')
                except PermissionError:
                    return None


class Hyperparams(hyperparams.Hyperparams):
    """
    Hyper-parameters for this primitive.
    """
    use_pretrained = hyperparams.UniformBool(
        default=True,
        description="Whether to use pre-trained ImageNet weights",
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter']
    )
    img_resize = hyperparams.Constant(
        default=224,
        description="Size to resize the input image",
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter']
    )


class CNNFeatureExtract(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A sample primitive to demonstrate the use of static files.
    """
    # Check if the weights directory exist, else create one. Default: /static
    WeightsDirPrimitive._weights_data_dir(dir_name="/code/static")
    __author__ = 'UBC DARPA D3M Team, Tony Joseph <tonyjos@cs.ubc.ca>'
    global _weights_configs
    _weights_configs = [{'type': 'FILE',
                         'key': 'vgg16-397923af.pth',
                         'file_uri': 'https://download.pytorch.org/models/vgg16-397923af.pth',
                         'file_digest': '397923af8e79cdbb6a7127f12361acd7a2f83e06b05044ddf496e83de57a5bf0'}
    ]
    metadata = hyperparams.base.PrimitiveMetadata({
        "id": "cnn-feature-extraction",
        "version": config.VERSION,
        "name": "CNN feature extraction",
        "description": "A primitive that makes use of pre-trained CNN to extract features",
        "python_path": "d3m.primitives.feature_extraction.cnn.UBC",
        "primitive_family": "FEATURE_EXTRACTION",
        "algorithm_types": ["CONVOLUTIONAL_NEURAL_NETWORK"],
        "source": {
            "name": config.D3M_PERFORMER_TEAM,
            "contact": config.D3M_CONTACT,
            "uris": [config.REPOSITORY]
        },
        "keywords": ["CNN", "VGG"],
        "installation": [config.INSTALLATION] + _weights_configs,
    })

    def __init__(self, *, hyperparams: Hyperparams, volumes: typing.Union[typing.Dict[str, str], None]=None) -> None:
        super().__init__(hyperparams=hyperparams, volumes=volumes)
        self.volumes = volumes
        self.hyperparams = hyperparams

        # Get weight path
        vgg_weights_path = self._find_weights_dir(key_filename='vgg16-397923af.pth', weights_configs=_weights_configs[0])
        self.vgg_model   = VGG(pretrained=self.hyperparams['use_pretrained'])
        if self.hyperparams['use_pretrained']:
            checkpoint = torch.load(vgg_weights_path)
            self.vgg_model.load_state_dict(checkpoint)
            logging.info("Pre-Trained imagenet weights loaded!")

        # Use GPU if available
        use_cuda = torch.cuda.is_available()
        self.device = torch.device("cuda:0" if use_cuda else "cpu")
        if use_cuda:
            self.vgg_model.to(self.device)


    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        text_columns  = inputs.metadata.get_columns_with_semantic_type('https://metadata.datadrivendiscovery.org/types/FileName') # [1]
        base_paths    = [inputs.metadata.query((metadata_base.ALL_ELEMENTS, t)) for t in text_columns] # Image Dataset column names
        base_paths    = [base_paths[t]['location_base_uris'][0].replace('file:///', '/') for t in range(len(base_paths))] # Path + media
        all_img_paths = [[os.path.join(base_path, filename) for filename in inputs.iloc[:,col]] for base_path, col in zip(base_paths, text_columns)]

        # Hyperparameters
        img_size = int(self.hyperparams['img_resize'])
        # Image pre-process before feature extraction
        pre_process = transforms.Compose([
            transforms.Resize(255),
            transforms.CenterCrop(img_size),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])]
        )
        # Set model to evaluate mode
        self.vgg_model.eval()

        features = []
        for idx in range(len(all_img_paths)):
            img_paths = all_img_paths[idx]
            for imagefile in img_paths:
                if os.path.isfile(imagefile):
                    image = Image.open(imagefile)
                    image = pre_process(image) # To pytorch tensor
                    image = image.unsqueeze(0) # 1 x C x H x W
                    feature = self.vgg_model(image.to(self.device))
                    feature = torch.flatten(feature)
                    feature = feature.data.cpu().numpy()
                else:
                    logging.warning("No such file {}. Feature vector will be set to all zeros.".format(file_path))
                    feature = np.zeros((512*7*7))
                # Collect features
                features.append(feature)

        outputs = container.DataFrame(features, generate_metadata=True)

        # Update Metadata for each feature vector column
        for col in range(outputs.shape[1]):
            col_dict = dict(outputs.metadata.query((metadata_base.ALL_ELEMENTS, col)))
            col_dict['structural_type'] = type(1.0)
            col_dict['name']            = "vector_" + str(col)
            col_dict["semantic_types"]  = ("http://schema.org/Float", "https://metadata.datadrivendiscovery.org/types/Attribute",)
            outputs.metadata            = outputs.metadata.update((metadata_base.ALL_ELEMENTS, col), col_dict)


        return base.CallResult(outputs)


    def _find_weights_dir(self, key_filename, weights_configs):
        _weight_file_path = None
        # Check common places
        if key_filename in self.volumes:
            _weight_file_path = self.volumes[key_filename]
        else:
            if os.path.isdir('/static'):
                _weight_file_path = os.path.join('/static', weights_configs['file_digest'], key_filename)
                if not os.path.exists(_weight_file_path):
                    _weight_file_path = os.path.join('/static', weights_configs['file_digest'])
            # Check other directories
            if not os.path.exists(_weight_file_path):
                home = expanduser("/")
                root = expanduser("~")
                _weight_file_path = os.path.join(home, weights_configs['file_digest'])
                if not os.path.exists(_weight_file_path):
                    _weight_file_path = os.path.join(home, weights_configs['file_digest'], key_filename)
                if not os.path.exists(_weight_file_path):
                    _weight_file_path = os.path.join('.', weights_configs['file_digest'], key_filename)
                if not os.path.exists(_weight_file_path):
                    _weight_file_path = os.path.join(root, weights_configs['file_digest'], key_filename)
                if not os.path.exists(_weight_file_path):
                    _weight_file_path = os.path.join(weights_configs['file_digest'], key_filename)

        if os.path.isfile(_weight_file_path):
            return _weight_file_path
        else:
            raise ValueError("Can't get weights file from the volume by key: {} or in the static folder: {}".format(key_filename, _weight_file_path))

        return _weight_file_path
