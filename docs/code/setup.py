from setuptools import setup

with open('requirements.txt', 'r') as f:
    install_requires = list()
    dependency_links = list()
    for line in f:
        re = line.strip()
        if re:
            install_requires.append(re)

setup(name='code',
      version='0.1.0',
      description='Sample build primitive build model',
      author='UBC',
      url='https://gitlab.com/tjoseph/code.git',
      maintainer_email='tonyjos@cs.ubc.ca',
      maintainer='Tony Joseph',
      license='MIT',
      packages=[
                'samplePrimitives',
                'samplePrimitives.samplePrimitive1',
                'samplePrimitives.samplePrimitive2',
               ],
      zip_safe=False,
      python_requires='>=3.6',
      install_requires=install_requires,
      keywords='d3m_primitive',
      entry_points={
          'd3m.primitives': [
              'classification.input_to_output.UBC=samplePrimitives.samplePrimitive1:InputToOutput',
              'feature_extraction.cnn.UBC=samplePrimitives.samplePrimitive2:CNNFeatureExtract',
          ],
      })
