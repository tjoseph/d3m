Tutorial
========

This tutorial assumes the reader is familiar with ``D3M`` system in general.
If not, please refer to the previous sections of this documentation.

**Note:** All the code tutorial below will be based on the `test code <https://gitlab.com/datadrivendiscovery/d3m/docs/code>`__ setup.


Overview of building a primitive
--------------------------------

1. Recognize what kind of primitive, based on the available `primitive interfaces <https://gitlab.com/datadrivendiscovery/d3m/tree/devel/d3m/primitive_interfaces>`__.

2. Identify the input and output `container <https://gitlab.com/datadrivendiscovery/d3m/tree/devel/d3m/container>`__ types.

3. Setup
   `metadata <https://gitlab.com/datadrivendiscovery/d3m/tree/devel/d3m/metadata>`__
   for each primitive.

4. Write a Unit Test to verify the primitive functions.

5. Generate the `json` file description for the primitive.

6. Write Pipeline for testing primitive functionality.


Primitive Class
---------------

There are a variety of primitve interfaces/class available. As an example, just to
have feature extraction without any fitting required, meaning to produce useful
outputs (features) from inputs (data) directly, ``TransformerPrimitiveBase``
from ``transformer.py`` can be used.

Each primitives can have it's own `hyperparameters <https://gitlab.com/datadrivendiscovery/d3m/blob/devel/d3m/metadata/hyperparams.py>`__. Some example types are:

``Constant, UniformBool, UniformInt, Choice, List``

Also, each hyperparameters should be defined as one of the four
`semantic types <https://docs.datadrivendiscovery.org/v2020.1.9/metadata.html#hyper-parameters>`__:

``https://metadata.datadrivendiscovery.org/types/TuningParameter``
``https://metadata.datadrivendiscovery.org/types/ControlParameter``
``https://metadata.datadrivendiscovery.org/types/ResourcesUseParameter``
``https://metadata.datadrivendiscovery.org/types/MetafeatureParameter``


**Sample Setup**

.. code:: python

    from d3m.primitive_interfaces import base, transformer
    from d3m.metadata import base as metadata_base, hyperparams

    __all__ = ('SampleTransform',)

    class Hyperparams(hyperparams.Hyperparams):
      """
      docstrings

      The docstrings here are very important and must to be included. It should
      contain relevant information of the hyperaparams/primtive functionality/etc.
      """
      learning_rate = hyperparams.Uniform(lower=0.0, upper=1.0, default=0.001, semantic_types=[
          'https://metadata.datadrivendiscovery.org/types/TuningParameter'
      ])
      clusters = hyperparams.UniformInt(lower=1, upper=100, default=10, semantic_types=[
          'https://metadata.datadrivendiscovery.org/types/TuningParameter'
      ])

    class SampleTransform(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
      """
      docstrings
      """
      def __init__(self, *, hyperparams: Hyperparams, volumes: typing.Union[typing.Dict[str, str], None]=None):
        super().__init__(hyperparams=hyperparams, volumes=volumes)
        self.hyperparams = hyperparams

      def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        pass


Input/Output type
-----------------

The acceptable input/outputs must be pre-defined. D3M supports a variety of
standard IO's such as:

- DataFrame :``pandas.DataFrame``

- ndarray :``numpy.ndarray``

- matrix :``numpy.matrix``

- List :``list``

**Note**:  Even thought they are standard types, the D3M types must be used for input/outputs. An example is shown below.

**Sample Setup**

.. code:: python

    from d3m import container

    Inputs  = container.DataFrame
    Outputs = container.DataFrame

    class SampleTransform(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
        """
        docstrings
        """
        __author__ = <Your-Name>
        metadata   = ...

        def __init__(self, *, hyperparams: Hyperparams, volumes: typing.Union[typing.Dict[str, str], None]=None):
            super().__init__(hyperparams=hyperparams, volumes=volumes)
           ....
        ...


**Note:** When returning the output dataframe, its metadata should be updated with the correct semantic and structural types.

Example:

.. code:: python

  # Update Metadata for each dataframe column
  for col in range(Outputs.shape[1]):
      col_dict = dict(Outputs.metadata.query((metadata_base.ALL_ELEMENTS, col)))
      col_dict['structural_type'] = type(1.0)
      col_dict['name']            = <name> + str(col)
      col_dict["semantic_types"]  = ("http://schema.org/Float", "https://metadata.datadrivendiscovery.org/types/Attribute",)
      Outputs.metadata            = Outputs.metadata.update((metadata_base.ALL_ELEMENTS, col), col_dict)


Metadata
--------

It is very crucial to set up the metadata for the primitive properly. ``D3M`` uses
the primitive's metadata to index and run TA-2 systems.

Below is a description of core metadata setup required for each primitive.

**Sample Setup**

.. code:: python

    from d3m.primitive_interfaces import base, transformer
    from d3m.metadata import base as metadata_base, hyperparams

    __all__ = ('SampleTransform',)

    class SampleTransform(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
        """
        docstrings
        """
        __author__ = <Your-Name>
        metadata = metadata_base.PrimitiveMetadata({
            'id': <Unique-ID, generated using UUID>,
            'version': <Primitive-development-version>,
            'name': <Primitive-Name>,
            'python_path': 'd3m.primitives.<>.<>' # Must match path in setup.py,
            'source': {
                'name': <Project-maintainer-name>,
                'uris': [<GitHub-link-to-project>],
                'contact': 'mailto:<Author E-Mail>'
            },
            'installation': [{
                'type': metadata_base.PrimitiveInstallationType.PIP,

                'package_uri': 'git+<GitHub-link-to-project>@{git_commit}#egg=<Project_Name>'.format(
                    git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
                ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.<Choose-the-algorithm-type-that-best-describes-the-primitive>,
                # Check https://metadata.datadrivendiscovery.org/devel/?definitions#definitions.algorithm_types for all available algorithm types.
                # If primitive family is not avaible a Merge Request should be made to add it to core package.
            ],
            'primitive_family': metadata_base.PrimitiveFamily.<Choose-the-primitive-familty-that-closely-associates-to-the-primitive>
            # Check https://metadata.datadrivendiscovery.org/devel/?definitions#definitions.primitive_family for all available primitive family types.
        })

        def __init__(self, *, hyperparams: Hyperparams, volumes: typing.Union[typing.Dict[str, str], None]=None):
          ....


Unit Test
------------

Once the primitives are constructed, unit testing must be done to see if the
primitive works as intended.

**Sample Setup**

.. code:: python

    import os
    import unittest

    from d3m.container.dataset import Dataset
    from d3m.metadata import base as metadata_base
    from common_primitives import dataset_to_dataframe

    from sample_primitive import SampleTransform

    class TestSampleTransform(unittest.TestCase):
      """
      docstrings
      """
      def test():
        # Load Some dataset,
        # Datasets can be obtained from: https://gitlab.datadrivendiscovery.org/d3m/datasets
        base_path = '.../datasets/seed_datasets_current/'
        dataset_doc_path = os.path.join(base_path, '38_sick_dataset', 'datasetDoc.json')
        dataset = Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))
        dataframe_hyperparams_class = dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.get_hyperparams()
        dataframe_primitive = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=dataframe_hyperparams_class.defaults())
        dataframe = dataframe_primitive.produce(inputs=dataset)

        # Call Primitive
        hyperparams_class = SampleTransform.metadata.get_hyperparams()
        primitive  = SampleTransform(hyperparams=hyperparams_class.defaults(), volumes=all_volumes)
        test_out   = primitive.produce(inputs=dataframe.value)

        # Write Checks to make sure that the output (type and shape) is whats Expected
        assert()

        ...

    if __name__ == '__main__':
      unittest.main()

It is recommended to do the testing inside the D3M docker container:

.. code:: shell

    docker run  -v ../local/path/code:/code
                -i -t registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9\
                /bin/bash

    cd /code/samplePrimitive1

    python3 primitive_name_test.py


Primitive `json` file description
---------------------------------

Once primitive is constructed and unit testing is successfully completed, the
final step in building a primitve is to generate the `json` file description
which will be indexed and used by D3M.

.. code:: shell

    docker run  --rm \
               -v ../local/path/code:/code\
               -i -t registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9\
               /bin/bash

    cd /code

    pip3 install -e .

    cd /samplePrimitive1

    python3 -m d3m index describe -i 4 <primitive_name>


or a `python file <https://gitlab.com/datadrivendiscovery/d3m/docs/code/samplePrimitives/generate-primitive-json.py>`__ can be written to generate the `json` file description as well.
This would be more convenient when having to manage multiple primitives.
In this case, generating the `json` primitive is shown as follows:

.. code:: shell

    docker run  --rm \
               -v ../local/path/code:/code
               -i -t registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9\
               /bin/bash

    cd /code

    pip3 install -e .

    python3 generate-primitive-json.py --args..


Pipeline
--------------
After building custom primitives, it has to put into a pipeline and validated using one of
``D3M seed_datasets`` in order to be integrated into the ``D3M primitives``.

The essential elements (outline) for building pipelines are:

``Dataset Loader -> Dataset Parser -> Data Cleaner (If necessary) -> Feature Extraction -> Classifier/Regression -> Output``

An example code of building pipeline is shown below:

.. code:: python

    # D3M dependencies
    from d3m import index
    from d3m.metadata import base as metadata_base
    from d3m.metadata.base import Context, ArgumentType
    from d3m.metadata.pipeline import Pipeline, PrimitiveStep

    # Common Primitives
    from common_primitives import construct_predictions
    from common_primitives.column_parser import ColumnParserPrimitive
    from common_primitives.dataset_to_dataframe import DatasetToDataFramePrimitive
    from common_primitives.extract_columns_semantic_types import ExtractColumnsBySemanticTypesPrimitive
    from common_primitives.remove_semantic_types import RemoveSemanticTypesPrimitive
    import d3m.primitives.data_cleaning.imputer as Imputer
    import d3m.primitives.classification.random_forest as RF

    # Testing primitive
    from samplePrimitives.samplePrimitive1.input_to_output import InputToOutput

    # Pipeline
    pipeline = Pipeline()
    pipeline.add_input(name='inputs')

    # Step 0: DatasetToDataFrame (Dataset Loader)
    step_0 = PrimitiveStep(primitive_description=DatasetToDataFramePrimitive.metadata.query())
    step_0.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='inputs.0')
    step_0.add_output('produce')
    pipeline.add_step(step_0)

    # Step 1: Custom primitive
    step_1 = PrimitiveStep(primitive=InputToOutput)
    step_1.add_argument(name='inputs',  argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
    step_1.add_output('produce')
    pipeline.add_step(step_1)

    # Step 2: Column Parser (Dataset Parser)
    step_2 = PrimitiveStep(primitive_description=ColumnParserPrimitive.metadata.query())
    step_2.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.1.produce')
    step_2.add_output('produce')
    pipeline.add_step(step_2)

    # Step 3: Extract Attributes (Feature Extraction)
    step_3 = PrimitiveStep(primitive_description=ExtractColumnsBySemanticTypesPrimitive.metadata.query())
    step_3.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.2.produce')
    step_3.add_output('produce')
    step_3.add_hyperparameter(name='semantic_types', argument_type=ArgumentType.VALUE, data=['https://metadata.datadrivendiscovery.org/types/Attribute'] )
    pipeline.add_step(step_3)

    # Step 4: Extract Targets (Dataset Parser)
    step_4 = PrimitiveStep(primitive_description=ExtractColumnsBySemanticTypesPrimitive.metadata.query())
    step_4.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
    step_4.add_output('produce')
    step_4.add_hyperparameter(name='semantic_types', argument_type=ArgumentType.VALUE, data=['https://metadata.datadrivendiscovery.org/types/TrueTarget'] )
    pipeline.add_step(step_4)

    attributes = 'steps.3.produce'
    targets    = 'steps.4.produce'

    # Step 6: Imputer (Data Cleaner)
    step_5 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.data_cleaning.imputer.SKlearn'))
    step_5.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=attributes)
    step_5.add_output('produce')
    pipeline.add_step(step_5)

    # Step 7: Classifier
    step_6 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.classification.decision_tree.SKlearn'))
    step_6.add_argument(name='inputs',  argument_type=ArgumentType.CONTAINER,  data_reference='steps.5.produce')
    step_6.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference=targets)
    step_6.add_output('produce')
    pipeline.add_step(step_6)

    # Final Output
    pipeline.add_output(name='output predictions', data_reference='steps.6.produce')

    # print(pipeline.to_json())
    with open('./pipeline.json', 'w') as write_file:
        write_file.write(pipeline.to_json(indent=4, sort_keys=False, ensure_ascii=False))


Once pipeline is constructed and the pipeline ``json`` file is generated, the pipeline is validated using ``python3 -m d3m runtime`` command (An example script is shown below).
Successfully running the pipeline validates that the primitive is working as intended.

.. code:: shell

    sudo docker run --rm\
                    -v ../local/path/code:/code\
                    -i -t registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 /bin/bash\
                    -c "cd /code;\
                        pip3 install -e .;\
                        cd pipelines;\
                        python3 -m d3m runtime fit-produce \
                                -p pipeline.json \
                                -r /datasets/seed_datasets_current/38_sick/TRAIN/problem_TRAIN/problemDoc.json \
                                -i /datasets/seed_datasets_current/38_sick/TRAIN/dataset_TRAIN/datasetDoc.json \
                                -t /datasets/seed_datasets_current/38_sick/TEST/dataset_TEST/datasetDoc.json \
                                -o 38_sick_results.csv \
                                -O pipeline_run.yml;\
                        exit"



Advanced: Primitive with static files
-------------------------------------

When building primitives that uses external/static files i.e. pre-trained weights, the
metadata for the primitive must be properly configured.
The static file can be hosted anywhere based on the user preference, as long as the URL to the file is a direct download link. It must
not have any security restrictions that prevent the ``d3m validation`` from accessing the file. Be sure not to change the static file URL, as
the older version of the primitive could potentially start failing.
A working `example <https://gitlab.com/datadrivendiscovery/d3m/docs/code/samplePrimitives/samplePrimitive2/feature_extraction.py>`__.

Below is a description of core metadata setup required, termed `_weights_configs` for
each static file.

**Sample Setup**

.. code:: python

    global _weights_configs
    _weights_configs = [{'type': 'FILE',
                         'key': '<Weight File Name>',
                         'file_uri': '<URL to directly Download the Weight File>',
                         'file_digest':'sha256sum of the <Weight File>'}
    ]


This `_weights_configs` should be directly added to the ``INSTALLATION`` parameter of the metadata.

.. code:: python

    from d3m.primitive_interfaces import base, transformer
    from d3m.metadata import base as metadata_base, hyperparams

    __all__ = ('SampleTransform',)

    class SampleTransform(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
        """
        docstrings
        """
        global _weights_configs
        _weights_configs = [{'type': 'FILE',
                             'key': '<Weight File Name>',
                             'file_uri': '<URL to directly Download the Weight File>',
                             'file_digest':'sha256sum of the <Weight File>'}
        ]

        __author__ = <Your-Name>
        metadata   =...
                    ...
                    ...
            'installation': [{
                'type': metadata_base.PrimitiveInstallationType.PIP,

                'package_uri': 'git+<GitHub-link-to-project>@{git_commit}#egg=<Project_Name>'.format(
                    git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
                ),
            }] + _weights_configs,
            ....

        def __init__(self, *, hyperparams: Hyperparams, volumes: typing.Union[typing.Dict[str, str], None]=None):
          ....

After the metadata setup, it is important to include code to return the path of files.
An example is given as follows:

.. code:: python

    def __init__(self, *, hyperparams: Hyperparams, volumes: typing.Union[typing.Dict[str, str], None]=None) -> None:
        super().__init__(hyperparams=hyperparams, volumes=volumes)
        self.volumes = volumes
        self.hyperparams = hyperparams

    def _find_weights_dir(self, key_filename, weights_configs):
        if key_filename in self.volumes:
            _weight_file_path = self.volumes[key_filename]
        else:
            _weight_file_path = os.path.join('.', weights_configs['file_digest'], key_filename)

        if os.path.isfile(_weight_file_path):
            return _weight_file_path
        else:
            raise ValueError("Can't get weights file from the volume by key: {} or in the static folder: {}".format(key_filename, _weight_file_path))

        return _weight_file_path

In this example code,  ``_find_weights_dir`` function will try to find the static files from volumes based on weight file name.
If it cannnot be found, then it looks into the default path, which is current path (``'.'``) followed by the ``file_digest`` and weight file name.

Running an `example <https://gitlab.com/datadrivendiscovery/d3m/docs/code/pipelines/primitive2_pipeline.py>`__ pipeline with static files is shown as follows:

.. code:: shell

    sudo docker run --rm\
                    -v .../local/path/code:/code\
                    -i -t registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 /bin/bash\
                    -c "cd /code;\
                        pip3 install -e .;\
                        cd pipelines;\
                        python3 primitive2_pipeline.py;\
                        mkdir /static
                        python3 -m d3m index download -p d3m.primitives.feature_extraction.cnn.UBC -o /static; \
                        python3 -m d3m runtime --volumes /static fit-produce \
                                -p feature_pipeline.json \
                                -r /datasets/seed_datasets_current/22_handgeometry/TRAIN/problem_TRAIN/problemDoc.json \
                                -i /datasets/seed_datasets_current/22_handgeometry/TRAIN/dataset_TRAIN/datasetDoc.json \
                                -t /datasets/seed_datasets_current/22_handgeometry/TEST/dataset_TEST/datasetDoc.json \
                                -o 22_handgeometry_results.csv \
                                -O feature_pipeline_run.yml;\
                        exit"


The static files will be downloaded and stored locally based on ``file_digest`` of `_weights_configs`.
In this way we don't duplicate same files used by multiple primitives.

::

    $ mkdir /static
    $ python3 -m d3m index download -p d3m.primitives.<>.<> -o /static;

``-p`` optional arugment to download static files for a particular primitive.

``-o`` optional arugment to download the static files into a common folder. If not they will be downloaded into default directory.

After the download, the file structure is given as follows:

::

    static
          <file_digest>
          <file_digest>
          ...
          ...

or if the local folder is not specified in ``index download``,  the default file structure:

::

    <file_digest>
             <Weight File>
      <file_digest>
            <Weight File>
          ...
          ...
