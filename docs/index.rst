D3M core package's documentation
================================

:Version: |version|

This is documentation for the common code for D3M project,
the ``d3m`` core package.

.. toctree::
   :maxdepth: 2

   installation
   interfaces
   discovery
   metadata
   pipeline
   reference
   tutorial

Miscellaneous pages
-------------------

* :ref:`about`
* :ref:`repostructure`

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
